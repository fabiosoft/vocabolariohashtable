//
//  LinkedHashEntry.cpp
//  vocabolarioHashTable
//
//  Created by Fabio Nisci on 05/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

#include "LinkedHashEntry.h"

LinkedHashEntry::LinkedHashEntry(int key, std::string term, std::string meaning) {
    this->key = key;
    this->term = term;
    this->next = NULL;
    this->meaning = meaning;
}

LinkedHashEntry::~LinkedHashEntry(){
    
}

int LinkedHashEntry::getKey() {
    return key;
}

std::string LinkedHashEntry::getTerm() {
    return this->term;
}

LinkedHashEntry* LinkedHashEntry::getNext() {
    return this->next;
}

std::string LinkedHashEntry::getMeaning(){
    return this->meaning;
}

void LinkedHashEntry::setTerm(std::string term) {
    this->term = term;
}

void LinkedHashEntry::setNext(LinkedHashEntry *next) {
    this->next = next;
}

void LinkedHashEntry::setKey(int key) {
    this->key = key;
}

void LinkedHashEntry::setMeaning(std::string meaning){
    this->meaning = meaning;
}