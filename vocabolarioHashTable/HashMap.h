//
//  HashMap.h
//  vocabolarioHashTable
//
//  Created by Fabio Nisci on 05/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

#include <iostream>
#include "LinkedHashEntry.h"

class HashMap {
private:
    //tabella che contiene ogni voce
    LinkedHashEntry **table;
public:
    //costruttore - distruttore
    HashMap();
    ~HashMap();
    //operazioni base
    LinkedHashEntry* get(std::string term);
    void put(std::string term, std::string meaning);
    void remove(std::string word);
    
    //stampa intero dizionario
    void printDictionary();
    //valore hash di una parola
    int hashWord(std::string word);
    //trova alternativa nel dizionario ad una parola
    LinkedHashEntry* getAlternative(LinkedHashEntry *word);

private:
    //distanza di Levenshtein tra due stringhe
    int DistanzaLev(std::string s,std::string t);
    //minimo tra tre interi
    int valore_minimo(int a,int b,int c);
    //stampa voci
    void printList(LinkedHashEntry *p);
};