//
//  main.cpp
//  vocabolarioHashTable
//
//  Created by Fabio Nisci on 05/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

/*
 (a) Costruire un vocabolario utilizzando una Hash Table con concatenamento, che abbia le seguenti funzioni:
 • Inserimento del termine
 • Cancellazione
 • Ricerca del termine. In caso di fallimento deve restituire una lista
 delle parole piu ́ prossime, utilizzando un approccio basato sulla Distanza di Levenshtein
 */

#include <iostream>
#include <ctype.h> /* contiene prototipo di isspace() */
#include "main.h"
#include "HashMap.h"

void mostraMenu(){
    std::cout <<
    "-------------------------------------------------\n"<<
    "| Progetto Algoritmi e Strutture Dati\n"<<
    "| Dizionario HashTable\n"<<
    "| Fabio Nisci - Matricola 0124000074 - 2012/2013\n"<<
    "-------------------------------------------------\n"<<
    "\n"
    "e word\t\t\t\tCancella dal dizionario la parola word.\n"<<
    "h word\t\t\t\tStampa il valore della funzione hash su word.\n"<<
    "i word meaning\t\t'parola significato' - Inserisce nel dizionario il termine con il significato.\n"<<
    "p    \t\t\t\tStampa la tabella hash.\n"<<
    "s word\t\t\t\tCerca e verifica la presenza di word  nel dizionario.\n"<<
    "q    \t\t\t\tTermina l'esecuzione del programma.\n";
}

int main(int argc, const char * argv[]){
    //mostra menu con tutti i comandi disponibili
    mostraMenu();
    
    //nuovo dizionario
    HashMap *dizionario = new HashMap();
    int c;
    std::string word,meaning,input;
    
    do{
        
        // cerca il primo carattere c diverso da spazio
        for( c=getchar() ; isspace(c);c=getchar() );
        switch(c){
            case 'e': // cancella dalla tabella la parola letta in input
                std::cin >> word;
                dizionario->remove(word);
                break;
                
            case 'h':  //  stampa il valore hashWord della parola letta in input
                std::cin >> word;
                std::cout << "HASH(" << word << ") = " << dizionario->hashWord(word) << "\n";
                break;
                
            case 'i':  // inserisci la parola letta in input
                //leggo l'intera linea di testo inserita
                //formato: i parola significato(con spazi)
                getline(std::cin, input);
                //ottengo la prima parola inserita
                word = getFirstWord(input);
                //ottengo solo il significato
                meaning = input.substr(word.length()+2);
                //inserisco parole e significato nel dizionario
                dizionario->put(word, meaning);
                
                break;
                
            case 'p': // stampa il contenuto della tabella
                dizionario->printDictionary();
                break;
                
            case 's': // verifica la presenza nel dizionario della parola letta in input
                std::cin >> word;
                //cerca la parola nel dizionario
                LinkedHashEntry *wordFound = dizionario->get(word);
                if (wordFound != NULL) {
                    //la parole è nel dizionario
                    std::cout << wordFound->getTerm() << " -> " << wordFound->getMeaning() << "\n";
                }else{
                    //se la parole non esiste cerco un alternativa nel dizionario
                    LinkedHashEntry *wordEntry = new LinkedHashEntry(dizionario->hashWord(word), word, "");
                    //trovo la parola più vicina a quella digitata con Levenshtein
                    LinkedHashEntry *alternativeWord = dizionario->getAlternative(wordEntry);
                    if (alternativeWord != NULL) {
                        std::cout << "Forse cercavi " << alternativeWord->getTerm() << "?\n";
                    }
                }
                break;
        }
    }while(c!='q' && c!= 'Q'); /* termina quando c vale 'q' oppure 'Q' */
    return 0;
}

//estrae la prima parola da una stringa
std::string getFirstWord(std::string text){
    bool firstWordFound = false;
    std::string firstWord = "";
    text = trim(text);
    for(int i = 0; i <= text.length() && firstWordFound == false; i++){
        if (text[i] == ' '){
            firstWordFound = true;
            for (int j = 0; j <= i; j++){
                firstWord += text[j];
            }
        }
    }
    return trim(firstWord);
}


// Elimina primo spazio da una stringa da sinistra
static inline std::string &ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
    return s;
}

// Elimina ultimo spazio da una stringa da destra
static inline std::string &rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
    return s;
}

// Elimina spazio in testa ed in coda
static inline std::string &trim(std::string &s) {
    return ltrim(rtrim(s));
}

