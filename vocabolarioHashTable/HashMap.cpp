//
//  HashMap.cpp
//  vocabolarioHashTable
//
//  Created by Fabio Nisci on 05/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

#include "HashMap.h"
#include <stdlib.h>

const int DICT_SIZE = 5000;

//alloca la tabella ed imposta tutte le voci a NULL
HashMap::HashMap() {
    this->table = new LinkedHashEntry*[DICT_SIZE];
    for (int i = 0; i < DICT_SIZE; i++)
        this->table[i] = NULL;
}

//distruttore - libera la memoria
HashMap::~HashMap() {
    for (int i = 0; i < DICT_SIZE; i++)
        if (this->table[i] != NULL) {
            LinkedHashEntry *prevEntry = NULL;
            LinkedHashEntry *entry = this->table[i];
            while (entry != NULL) {
                prevEntry = entry;
                entry = entry->getNext();
                delete prevEntry;
            }
        }
    delete[] this->table;
}

//visualizza l'intero dizionario
void HashMap::printDictionary(){
    std::cout << "\nContenuto del dizionario:\n";
    for (int k = 0; k<DICT_SIZE; k++) {
        LinkedHashEntry *entry = this->table[k];
        if (entry != NULL) {
            std::cout << "[" << k << "] -> ";
            printList(entry);
        }
    }
}

/* Stampa tutte le parole  nella voce p, una parola per linea */
void HashMap::printList(LinkedHashEntry *p){
    for( ; p!= NULL; p=p->getNext())
        std::cout << p->getTerm() << ": " << p->getMeaning() << "\n";
}

//
LinkedHashEntry* HashMap::get(std::string term) {
    int hash = hashWord(term);
    if (this->table[hash] == NULL)
        return NULL;
    else {
        LinkedHashEntry *entry = this->table[hash];
        while (entry != NULL && entry->getKey() != hash)
            entry = entry->getNext();
        if (entry == NULL)
            return NULL;
        else
            return entry;
    }
}

void HashMap::put(std::string term, std::string meaning) {
    int hash = hashWord(term);
    if (this->table[hash] == NULL)
        this->table[hash] = new LinkedHashEntry(hash, term, meaning);
    else {
        LinkedHashEntry *entry = this->table[hash];
        while (entry->getNext() != NULL)
            entry = entry->getNext();
        if (entry->getKey() == hash)
            entry->setTerm(term);
        else
            entry->setNext(new LinkedHashEntry(hash, term, meaning));
    }
}

void HashMap::remove(std::string word) {
    int hash = hashWord(word);
    if (this->table[hash] != NULL) {
        LinkedHashEntry *prevEntry = NULL;
        LinkedHashEntry *entry = this->table[hash];
        while (entry->getNext() != NULL && entry->getKey() != hash) {
            prevEntry = entry;
            entry = entry->getNext();
        }
        if (entry->getKey() == hash) {
            if (prevEntry == NULL) {
                LinkedHashEntry *nextEntry = entry->getNext();
                delete entry;
                this->table[hash] = nextEntry;
            } else {
                LinkedHashEntry *next = entry->getNext();
                delete entry;
                prevEntry->setNext(next);
            }
        }
    }
}


/****  CALCOLO FUNZIONE HASH (METODO DELLA DIVISIONE)
 
 Il valore Val(w)  di una parola w  e' calcolato interpretando w
 come un numero in base 27, dove la cifra 'a' vale  1,  'b' vale  2,
 'c' vale 3, ... , 'z' vale 26. Ad esempio:
 
 Val("abc") = 1 * 27^2 + 2 * 27^1 + 3 * 27 ^0 = 729 + 54 + 3 = 786
 
 Si e' scelto di far valere 'a' 1 anziche' 0 altrimenti le parole
 "a", "aa", "aaa", ... avrebbero tutte lo stesso valore 0.
 In questo modo invece parole distinte hanno valori distinti (inoltre, dato un
 numero che rappresenta una parola w, si puo' ricavare w).
 */
int HashMap::hashWord(std::string word){
    int val = 0;
    for(std::string::size_type i = 0; i < word.size(); ++i) {
        val = (27 * val + word[i] ) % DICT_SIZE;
    }
    return val;
}

/* Cerca alternativa in tutto il dizionario */
LinkedHashEntry* HashMap::getAlternative(LinkedHashEntry *word){
    
    int min = 100;  // iniziallizzo il minimo a 100 così da sostituirlo appena trova la parola
    int distLev;    // distanza di lev
    LinkedHashEntry *alternative;//entry da ritornare
    
    //da 0 a DICT_SIZE
    for(int k=0;k<DICT_SIZE; k++)
        //se è presente la parola
        if(this->table[k]!=NULL){
            LinkedHashEntry *entry = this->table[k];
            // per ogni elemento del dizionario
            // calcolo la distanza tra word e il k-esimo elemento (word)
            distLev = DistanzaLev(word->getTerm(), entry->getTerm());
            // se la distanza è minore del minimo
            if (distLev < min) {
                // la distanza calcolata sarà il nuovo minimo
                min = distLev;
                // e l'i-esimo elemento del dizionario sarà la parola alternativa
                alternative = new LinkedHashEntry(entry->getKey(), entry->getTerm(), entry->getMeaning());
            }
        }
    // ritorno l'alternativa
    return alternative;
}


/* Funzione per il calcolo effettivo della distanza di Levenshtein tra due Stringhe */
int HashMap::DistanzaLev(std::string s,std::string t){
    
    size_t s_lung=s.size(); //num caratteri di s
    size_t t_lung=t.size(); //num caratteri di t
    size_t R=s_lung+1;
    size_t C=t_lung+1;
    int matrice[R][C]; //la matrice (s+1)*(t+1)
    int valore_spostamento;
    
    for(int i=0;i<R;i++)
        // la prima riga della mat. conterrà le distanze da 0 a R
        // la distanza 1 è associata al primo chr della stringa, 0 al vuoto
        matrice[i][0]= i;
    for(int i=0;i<C;i++)
        // la prima colonna della mat. conterrà le distanze da 0 a C
        // la distanza 1 è associato al primo chr della stringa, 0 al vuoto
        matrice[0][i]= i;
    
    //calcolo valori di spostamento (lettere diverse)
    for(int r=1;r<R;r++){
        for(int c=1;c<C;c++){
            if(s[c]==t[r])
                // Se i due elementi sono uguali
                //il volore è 0
                valore_spostamento=0;
            else
                //altrimenti è 1
                valore_spostamento=1;
            
            
            // imposto la cella d[r][c] scegliendo il valore minimo tra:
            //   - la cella immediatamente superiore + 1
            //   - la cella immediatamente a sinistra + 1
            //   - la cella diagonalmente in alto a sinistra più il costo
            int v1=matrice[r -1][c] + 1;
            int v2=matrice[r][c -1] + 1;
            int v3=matrice[r - 1][c -1]  +  valore_spostamento  ;
            int v=valore_minimo(v1,v2,v3);
            matrice[r][c] = v;
        }
    }    
    //l'ultimo elemento rappresenta la distanza di Levenshtein
    return matrice[R-1][C-1];
}

/* Calcola il minimo tra 3 valori */
int HashMap::valore_minimo(int a,int b,int c){
    int min=a;
    
    if(b<min)
        min=b;
    if(c<min)
        min=c;
    
    return min;
}