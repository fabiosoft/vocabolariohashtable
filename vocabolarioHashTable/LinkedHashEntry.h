//
//  LinkedHashEntry.h
//  vocabolarioHashTable
//
//  Created by Fabio Nisci on 05/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

#include <iostream>

class LinkedHashEntry {
    
private:
    //chiave
    int key;
    //termine
    std::string term;
    //significato
    std::string meaning;
    //prossimo - hash table concatenata
    LinkedHashEntry *next;
public:
    //costruttore - distruttore
    LinkedHashEntry(int key, std::string term, std::string meaning);
    ~LinkedHashEntry();
    
    //getters
    int getKey();
    std::string getTerm();
    std::string getMeaning();
    LinkedHashEntry *getNext();
    
    //setters
    void setTerm(std::string term);
    void setMeaning(std::string meaning);
    void setNext(LinkedHashEntry *next);
    void setKey(int key);
};

