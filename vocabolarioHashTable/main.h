//
//  main.h
//  vocabolarioHashTable
//
//  Created by Fabio Nisci on 05/01/13.
//  Copyright (c) 2013 Fabiosoft. All rights reserved.
//

void mostraMenu();
//estrae la prima parola da una stringa
std::string getFirstWord(std::string text);
// Elimina primo spazio da una stringa da sinistra
static inline std::string &ltrim(std::string &s);
// Elimina ultimo spazio da una stringa da destra
static inline std::string &rtrim(std::string &s);
// Elimina spazio in testa ed in coda
static inline std::string &trim(std::string &s);